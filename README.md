# Seen Supertenant

master:
[![pipeline status](https://gitlab.com/seen/angular/seen-supertenant/badges/master/pipeline.svg)](https://gitlab.com/seen/angular/seen-supertenant/commits/master) 
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/5c26537417a64fe49c9de003b2d972c3)](https://www.codacy.com/app/seen/seen-supertenant?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=seen/angular/seen-supertenant&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/5c26537417a64fe49c9de003b2d972c3)](https://www.codacy.com/app/seen/seen-supertenant?utm_source=gitlab.com&utm_medium=referral&utm_content=seen/angular/seen-supertenant&utm_campaign=Badge_Coverage)

develop:
[![pipeline status - develop](https://gitlab.com/seen/angular/seen-supertenant/badges/develop/pipeline.svg)](https://gitlab.com/seen/angular/seen-supertenant/commits/develop)

This module provide objects and API to work with servers which implements Seen Supertenant API. It contains entities such as Product, Service, Order, OrderItem and their related APIs.

